//
//  ActionGroup.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 06/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

/// An ActionGroup encapsulates a collection of Action instances with a title that's used as the section title on the HomeViewController.
class ActionGroup: NSObject {
    var title: String
    var actions: [Action]
    
    init(title: String, actions: [Action]) {
        self.title = title
        self.actions = actions
    }

}
