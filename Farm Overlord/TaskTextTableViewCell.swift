//
//  TaskTextTableViewCell.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 12/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

class TaskTextTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    func setupCellWithText(text: String, role: EmployeeRole) {
        if role == EmployeeRole.Admin {
            self.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        
        self.titleLabel.text = text
    }
}
