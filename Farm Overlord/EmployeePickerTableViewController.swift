//
//  EmployeePickerTableViewController.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 12/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

protocol EmployeePickerDelegate {
    /**
    Called on the delegate instance once the user has selected a employee from the list of employees.
    
    - parameter employeePicker: A instance of the EmployeePickerTableViewController from which the delegate call originates from.
    - parameter employee:       A instance of the Employee class representing the chosen Employee.
    */
    func employeePicker(employeePicker: EmployeePickerTableViewController, didSelectEmployee employee: Employee)
}

/// A Helper class which allows the user to select a Employee from all of the employee listed in the Db.
class EmployeePickerTableViewController: UITableViewController {
    var employees = [Employee]()
    
    weak var currentlySelectedEmployee: Employee?
    var delegate: EmployeePickerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Employee.getAll({ (employees) -> (Void) in
            self.employees = employees
            self.tableView.reloadData()
            
            var index = 0
            for i in 0...self.employees.count - 1 {
                let employee = self.employees[i]
                
                if employee.employeeId == self.currentlySelectedEmployee?.employeeId {
                    index = i
                }
            }
            
            self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0), animated: false, scrollPosition: UITableViewScrollPosition.None)
        })
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.employees.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("employeeCell", forIndexPath: indexPath)
        let employee = self.employees[indexPath.row]
        
        cell.textLabel?.text = employee.username
        cell.detailTextLabel?.text = employee.role == EmployeeRole.Admin ? "Admin" : "User"
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let employee = self.employees[indexPath.row]
        
        if let delegate = self.delegate {
            delegate.employeePicker(self, didSelectEmployee: employee)
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Employees"
    }
}
