//
//  TaskTableViewCell.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 11/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
    @IBOutlet weak var viewStatus: UIView!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.viewStatus.layer.shadowColor = UIColor.blackColor().CGColor
        self.viewStatus.layer.shadowOpacity = 0.2
        self.viewStatus.layer.shadowRadius = 2.0
        self.viewStatus.layer.shadowOffset = CGSizeMake(0.0, 0.0)
    }
    
    func setupCellWithTask(task: Task) {
        self.labelTitle.text = task.title
        self.labelDescription.text = task.taskDescription
        
        switch(task.status) {
        case .ToDo:
            self.viewStatus.backgroundColor = UIColor.redColor()
            break;
        case .InProgress:
            self.viewStatus.backgroundColor = UIColor.yellowColor()
            break;
        case .Completed:
            self.viewStatus.backgroundColor = UIColor.greenColor()
            break;
        }
    }
}
