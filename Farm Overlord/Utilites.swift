//
//  Utilites.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 06/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

class Utilites: NSObject {
    /**
    Shows a UIAlertController configured with the passed in arguments.
    
    - parameter title:     The title of the UIAlertController.
    - parameter message:   The message of the UIAlertController.
    - parameter style:     The UIAlertControllerStyle of the UIAlertController.
    - parameter actions:   An array of UIAlertAction objects to add to the UIAlertController.
    - parameter presenter: A UIViewController object which will present the UIAlertController.
    - note If the presenter's nil, the method will use the window's "rootViewController".
    */
    class func showAlertController(title: String, message: String, style: UIAlertControllerStyle, actions: [UIAlertAction], presenter: UIViewController?) {
        var containsCancelAction = false
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        
        for action: UIAlertAction in actions {
            alertController.addAction(action)
            
            if action.style == UIAlertActionStyle.Cancel {
                containsCancelAction = true
            }
        }
        
        if !containsCancelAction {
            let cancelAction = UIAlertAction(title: style == UIAlertControllerStyle.ActionSheet ? "CANCEL" : "OK", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
                // Has to have a handler to work.
            })
            
            alertController.addAction(cancelAction)
        }
        
        guard presenter != nil else {
            UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
            return;
        }
        
        presenter?.presentViewController(alertController, animated: true, completion: nil)
    }
    
    /**
    Retrieves all of the ActionGroup instances that are available for a certain EmployeeRole.
    
    - parameter role: The role to retrieve available ActionGroup instances.
    
    - returns: Returns a collection of ActionGroup instances.
    - note: Currently, the collection cannot be empty.
    */
    class func actionGroupsForRole(role: EmployeeRole) -> [ActionGroup] {
        let tasks = Action(title: "Tasks", controllerId: "tasksViewController")
        let employeeManagement = Action(title: "Employee management", controllerId: "employeeManagementViewController")
        
        let normalGroup = ActionGroup(title: "", actions: [tasks])
        let adminGroup = ActionGroup(title: "Admin", actions: [employeeManagement])
        
        return role == EmployeeRole.Admin ? [normalGroup, adminGroup] : [normalGroup]
    }
    
    /**
    A wrapped method used to make calling dispatch_after a trivial task.
    
    - parameter delay:   The time after which to dispatch.
    - parameter closure: The code execute in the dispatch.
    */
    class func delay(delay: Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
}
