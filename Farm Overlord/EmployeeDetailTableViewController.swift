//
//  EmployeeDetailViewController.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 06/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

protocol EmployeeDetailDelegate {
    /**
    Called on the delegate once the specified employee's been removed from the database.
    
    - parameter employee: The delete Employee instance.
    - note: The method's invoked on the delegate AFTER the database operation and ONLY if the operation completed successfully.
    */
    func didDeleteEmployee(employee: Employee)
    
    /**
    Called on the delegate once the specified employee's data has been updated in the database.
    
    - parameter updatedEmployee: The updated Employee instance.
    - note: The method's invoked on the delegate AFTER the database operation and ONLY if the operation completed successfully.
    */
    func didUpdateEmployee(updatedEmployee: Employee)
    
    /**
    Called on the delegate once a new Employee instance has been inserted into the database.
    
    - parameter employee: The added Employee instance.
    - note: The method's invoked on the delegate AFTER the database operation and ONLY if the operation completed successfully.
    */
    func didAddEmployee(employee: Employee)
}

class EmployeeDetailTableViewController: UITableViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var labelRole: UILabel!
    
    @IBOutlet weak var rolePicker: UIPickerView!
    @IBOutlet weak var buttonAction: UIButton!
    
    var delegate: EmployeeDetailDelegate?
    weak var employee: Employee? {
        didSet {
            if self.didLoad {
                self.updateGUI()
            }
        }
    }
    
    var didLoad: Bool = false
    
    /// Determines whether the table view should expand the row hosting the status picker.
    var didOpenPicker: Bool = false
    
    var editBarButtonItem: UIBarButtonItem?
    var doneBarButtonItem: UIBarButtonItem?
    
    var textfields: [UITextField] {
        get {
            return [self.usernameTextField, self.firstNameTextField, self.lastNameTextField, self.emailTextField, self.passwordTextField]
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.didLoad = true
        
        self.navigationItem.title = "Employee details"
        self.navigationItem.leftItemsSupplementBackButton = true
        self.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
        
        self.editBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Edit, target: self, action: Selector("buttonEditTapped:"))
        self.doneBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: Selector("buttonDoneTapped:"))
        
        for textfield in self.textfields {
            textfield.delegate = self
        }
        
        self.usernameTextField.tag = 32
        self.firstNameTextField.tag = 32
        self.lastNameTextField.tag = 32
        self.emailTextField.tag = 320
        self.passwordTextField.tag = 32
        
        self.updateGUI()
    }
    
    // MARK: -
    // MARK: UIPickerViewDataSource
    // MARK: -
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Employee.roles().count
    }
    
    // MARK: -
    // MARK: UIPickerViewDelegate
    // MARK: -
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Employee.roles()[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.labelRole.text = Employee.roles()[row]
        self.rolePicker.hidden = true
        
        self.didOpenPicker = false
        
        // Initiate the table view updates without triggering a full reload. This will update thet status picker view cell height.
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
    
    // MARK: -
    // MARK: UITextFieldDelegate
    // MARK: -
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if (range.length + range.location > textField.text?.characters.count) {
            return false;
        }
        
        let newLength = textField.text!.characters.count + string.characters.count - range.length
        
        return newLength <= textField.tag
    }
    
    // MARK: -
    // MARK: UITableViewDelegate
    // MARK: -
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 6 {
            if self.didOpenPicker {
                return 75
            } else {
                return 0
            }
        } else {
            return 44
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if !self.tableView.editing && self.employee != nil {
            return
        }
        
        if indexPath.row == 5 {
            self.didOpenPicker = !self.didOpenPicker
            self.rolePicker.hidden = false
            
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    // MARK: -
    // MARK: Actions
    // MARK: -
    
    @IBAction func buttonActionTapped(sender: UIButton) {
        self.deleteEmployee()
    }
    
    func buttonEditTapped(sender: UIBarButtonItem) {
        self.tableView.editing = true
        self.updateGUI()
    }
    
    func buttonDoneTapped(sender: UIBarButtonItem) {
        self.tableView.editing = false
        self.updateEmployee()
        self.updateGUI()
    }
    
    // MARK: -
    // MARK: Helpers
    // MARK: -
    
    func updateGUI() {
        if self.employee == nil {
            self.showAddForm()
        } else {
            self.showUpdateForm()
        }
    }
    
    func addEmployee() {
        let employee = self.validateInsertion()
        
        if employee != nil {
            employee?.exists({ (success) -> (Void) in
                
                if !success {
                    employee?.insert({ (success) -> Void in
                        if success {
                            self.delegate?.didAddEmployee(employee!)
                            self.cleanupForm()
                            
                            Utilites.showAlertController("Creation error", message: "Successfully created the account.", style: UIAlertControllerStyle.Alert, actions: [], presenter: self)
                        } else {
                            Utilites.showAlertController("Creation error", message: "There was an error creating the account, please try again. If the error persists, contact your administrator.", style: UIAlertControllerStyle.Alert, actions: [], presenter: self)
                        }
                    })
                } else {
                    print("User already exists.")
                    Utilites.showAlertController("Creation error", message: "A employee with the specified username or email already exists.", style: UIAlertControllerStyle.Alert, actions: [], presenter: self)
                }
            })
        }
    }
    
    func deleteEmployee() {
        if self.employee?.employeeId == AppState.sharedInstance.signedInUser?.employeeId {
            Utilites.showAlertController("Deletion", message: "You cannot delete the signed in account.", style: UIAlertControllerStyle.Alert, actions: [], presenter: self)
            return
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (action) -> Void in }
        let deleteAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.Destructive) { (action) -> Void in
            print("Deleting employee.")
            
            self.employee!.remove({ (success) -> Void in
                if success {
                    self.delegate?.didDeleteEmployee(self.employee!)
                } else {
                    Utilites.showAlertController("Deletion failed", message: "Failed deleting \(self.employee!.firstName) \(self.employee!.lastName). Please try again and, if the problem persists, contact your administrator (Adminception).", style: UIAlertControllerStyle.Alert, actions: [], presenter: self)
                }
            })
        }
        
        Utilites.showAlertController("Delete", message: "Are you sure you want to delete \(employee!.firstName) \(employee!.lastName)?", style: UIAlertControllerStyle.Alert, actions: [cancelAction, deleteAction], presenter: self)
    }
    
    func updateEmployee() {
        if self.validateEditing() {
            let updatedEmployee = Employee(employeeId: self.employee!.employeeId, username: self.usernameTextField.text!, firstName: self.firstNameTextField.text!, lastName: self.lastNameTextField.text!, email: self.emailTextField.text!, password: self.passwordTextField.text!, role: self.employee!.role, creationDate: nil)
            
            self.employee?.update(updatedEmployee, completion: { (success) -> Void in
                if success {
                    print("Successfully updated the employee.")
                    
                    self.employee = updatedEmployee
                    self.delegate?.didUpdateEmployee(self.employee!)
                } else {
                    print("Failed updating the employee.")
                }
            })
        } else {
            print("The edit was invalid.")
        }
    }
    
    func validateInsertion() -> Employee? {
        guard !self.usernameTextField.text!.isEmpty else {
            self.showFormValidationError("The username field cannot be empty.")
            return nil
        }
        
        guard !self.firstNameTextField.text!.isEmpty else {
            self.showFormValidationError("The first name field cannot be empty.")
            return nil
        }
        
        guard !self.lastNameTextField.text!.isEmpty else {
            self.showFormValidationError("The last name field cannot be empty.")
            return nil
        }
        
        guard !self.emailTextField.text!.isEmpty else {
            self.showFormValidationError("The email field cannot be empty.")
            return nil
        }
        
        guard self.validateEmail(true) else {
            self.showFormValidationError("The email's invalid.")
            return nil
        }
        
        guard Employee.roles().contains(self.labelRole.text!) else {
            self.showFormValidationError("The role field has to be set.")
            return nil
        }
        
        guard !self.passwordTextField.text!.isEmpty else {
            self.showFormValidationError("The password field cannot be empty.")
            return nil
        }
        
        let role = self.labelRole.text == "User" ? EmployeeRole.User : EmployeeRole.Admin
        
        return Employee(employeeId: 0, username: self.usernameTextField.text!, firstName: self.firstNameTextField.text!, lastName: self.lastNameTextField.text!, email: self.emailTextField.text!, password: self.passwordTextField.text!, role: role, creationDate: nil)
    }
    
    func validateEmail(strict: Bool) -> Bool {
        let stricterFilter = true
        let stricterFilterString = "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$"
        let laxString = "^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$"
        let emailRegex = stricterFilter ? stricterFilterString : laxString
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        
        return emailTest.evaluateWithObject(self.emailTextField.text!)
    }
    
    func showFormValidationError(message: String) {
        Utilites.showAlertController("Form error", message: message, style: UIAlertControllerStyle.Alert, actions: [], presenter: self)
    }
    
    func validateEditing() -> Bool {
        guard self.employee!.username == self.usernameTextField.text else {
            return true
        }
        
        guard self.employee!.firstName == self.firstNameTextField.text else {
            return true
        }
        
        guard self.employee!.lastName == self.lastNameTextField.text else {
            return true
        }
        
        guard self.employee!.email == self.emailTextField.text else {
            return true
        }
        
        //        guard self.employee!.role == self.roleTextField.text else {
        //            return true
        //        }
        
        guard self.employee!.password == self.passwordTextField.text else {
            return true
        }
        
        return false
    }
    
    func showAddForm() {
        self.navigationItem.setRightBarButtonItem(nil, animated: true)
        
        self.buttonAction.setTitle("Create new account", forState: UIControlState.Normal)
        self.buttonAction.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal)
        self.buttonAction.removeTarget(self, action: Selector("deleteEmployee"), forControlEvents: UIControlEvents.TouchUpInside)
        self.buttonAction.addTarget(self, action: Selector("addEmployee"), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.cleanupForm()
    }
    
    func showUpdateForm() {
        self.buttonAction.setTitle("Delete employee", forState: UIControlState.Normal)
        self.buttonAction.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        self.buttonAction.removeTarget(self, action: Selector("addEmployee"), forControlEvents: UIControlEvents.TouchUpInside)
        self.buttonAction.addTarget(self, action: Selector("deleteEmployee"), forControlEvents: UIControlEvents.TouchUpInside)
        
        if self.tableView.editing {
            self.navigationItem.setRightBarButtonItems([self.doneBarButtonItem!], animated: true)
        } else {
            self.navigationItem.setRightBarButtonItems([self.editBarButtonItem!], animated: true)
        }
        
        for textfield in self.textfields {
            textfield.enabled = self.tableView.editing
        }
        
        self.usernameTextField.text = self.employee?.username != nil ? employee?.username : "N/A"
        self.firstNameTextField.text = self.employee?.firstName != nil ? employee?.firstName : "N/A"
        self.lastNameTextField.text = self.employee?.lastName != nil ? employee?.lastName : "N/A"
        self.emailTextField.text = self.employee?.email != nil ? employee?.email : "N/A"
        self.passwordTextField.text = self.employee?.password != nil ? employee?.password : "N/A"
        self.labelRole.text = self.employee?.stringForRole()
    }
    
    func cleanupForm() {
        self.labelRole.text = "Pick a role"
        
        for textfield in self.textfields {
            textfield.text = ""
            textfield.enabled = true
        }
        
        self.usernameTextField.becomeFirstResponder()
    }
    
    deinit {
        print("Deallocating the EmployeeDetailTableViewController.")
    }
}
