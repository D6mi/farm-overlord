//
//  TaskStatusTableViewCell.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 12/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

class TaskStatusTableViewCell: UITableViewCell {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var textFieldCompletionTime: UITextField!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.viewStatus.layer.shadowColor = UIColor.blackColor().CGColor
        self.viewStatus.layer.shadowOpacity = 0.2
        self.viewStatus.layer.shadowRadius = 2.0
        self.viewStatus.layer.shadowOffset = CGSizeMake(0.0, 0.0)
    }
    
    func setupCellWithTask(task: Task?, role: EmployeeRole, delegate: UITextFieldDelegate) {
        let estimatedCompletionTime = task == nil ? 0 : task!.estimatedCompletionTime
        let status = task == nil ? TaskStatus.ToDo : task!.status
        
        let numberFormatter = NSNumberFormatter()
        self.textFieldCompletionTime.text = numberFormatter.stringFromNumber(estimatedCompletionTime)
        
        self.textFieldCompletionTime.delegate = delegate
        self.textFieldCompletionTime.enabled = role == .Admin
        
        switch(status) {
        case .ToDo:
            self.labelTitle.text = "To do"
            self.viewStatus.backgroundColor = UIColor.redColor()
            break
        case .InProgress:
            self.labelTitle.text = "In progress"
            self.viewStatus.backgroundColor = UIColor.yellowColor()
            break
        case .Completed:
            self.labelTitle.text = "Completed"
            self.viewStatus.backgroundColor = UIColor.greenColor()
            break
        }
    }
    
}
