//
//  Action.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 06/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

/// Represents all of the actions accessible from the HomeViewController for a given EmployeeRole.
/// - note: The title's used as the text on the HomeViewController's cells and the controllerId to initiate an action on didSelectRow.
class Action: NSObject {
    var title: String
    var controllerId: String
    
    init(title: String, controllerId: String) {
        self.title = title
        self.controllerId = controllerId
        
        super.init()
    }

}
