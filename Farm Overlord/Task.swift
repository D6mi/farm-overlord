//
//  Task.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 01/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

enum TaskStatus : Int {
    case ToDo
    case InProgress
    case Completed
}

public struct TaskSQLConstants {
    static let TaskColumnNameID = "id"
    static let TaskColumnNameEmployeeId = "employee_id"
    static let TaskColumnNameTitle = "title"
    static let TaskColumnNameDescription = "description"
    static let TaskColumnNameStatus = "status"
    static let TaskColumnNameEstimatedCompletionTime = "estimated_completion_time"
    static let TaskColumnNameCreationDate = "creation_date"
    
    static let TaskInsert = "INSERT INTO tasks (\(TaskSQLConstants.TaskColumnNameTitle), \(TaskSQLConstants.TaskColumnNameEmployeeId), \(TaskSQLConstants.TaskColumnNameDescription), \(TaskSQLConstants.TaskColumnNameStatus), \(TaskSQLConstants.TaskColumnNameEstimatedCompletionTime)) VALUES  (?, ?, ?, ?, ?)"
    
    static let TaskSelectAll = "SELECT tasks.id as task_id, employee_id, title, description, status, estimated_completion_time, tasks.creation_date as task_creation_date, employees.id, username, first_name, last_name, email, password, role, employees.creation_date FROM tasks LEFT JOIN employees ON tasks.employee_id = employees.id"
    
}

class Task: NSObject {
    var taskId: Int
    
    var employeeId: Int = 0
    var employee: Employee? {
        willSet(employee) {
            if employee != nil {
                self.employeeId = employee!.employeeId
            }
        }
    }
    
    var title: String
    var taskDescription: String
    var status: TaskStatus
    var estimatedCompletionTime: Double
    var creationDate: NSDate?
    
    
    // MARK: -
    // MARK: Initializers
    // MARK: -
    
    /**
    A default initializer that initializes  a new Task instance with default values.
    
    - returns: Returns a newly initialized Task instance.
    */
    override init() {
        self.taskId = 0
        self.title = "Title not specified"
        self.taskDescription = "Task description not specified"
        self.status = TaskStatus.ToDo
        self.estimatedCompletionTime = 0
    }
    
    init(id: Int, employeeId: Int, title: String, taskDescription: String, status: TaskStatus, estimateCompletionTime: Double, creationDate: NSDate?) {
        self.taskId = id
        self.employeeId = employeeId
        self.title = title
        self.taskDescription = taskDescription
        self.status = status
        self.estimatedCompletionTime = estimateCompletionTime
        
        if let date = creationDate as NSDate! {
            self.creationDate = date
        } else {
            self.creationDate = NSDate()
        }
    }
    
    init(result: FMResultSet) {
        self.taskId = Int(result.intForColumn("task_id"))
        self.employeeId = Int(result.intForColumn(TaskSQLConstants.TaskColumnNameEmployeeId))
        self.title = result.stringForColumn(TaskSQLConstants.TaskColumnNameTitle)
        self.taskDescription = result.stringForColumn(TaskSQLConstants.TaskColumnNameDescription)
        self.status = TaskStatus(rawValue: Int(result.intForColumn(TaskSQLConstants.TaskColumnNameStatus)))!
        self.estimatedCompletionTime = Double(result.doubleForColumn(TaskSQLConstants.TaskColumnNameEstimatedCompletionTime))
        self.creationDate = result.dateForColumn("task_creation_date")
        
        let employeeId: Int? = Int(result.intForColumn(EmployeeSQLConstants.EmployeeColumnNameID))
        
        if employeeId != 0 {
            self.employee = Employee(result: result)
        }
    }
    
    // MARK: -
    // MARK: Database
    // MARK: -
    
    /**
    Retrieves all Tasks existing in the Db.
    
    - parameter completion: A completion block called upon operation completion.
    - note: The completion block is called on the main thread.
    */
    class func getAll(completion: ((tasks: [Task]) -> (Void))?) {
        let getAll = QueryOperation(sql: TaskSQLConstants.TaskSelectAll, arguments: []) { (result, error) -> Void in
            var tasks = [Task]()
            
            while(result.next()) {
                let task = Task(result: result)
                tasks.append(task)
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let completion = completion {
                    completion(tasks: tasks)
                }
            })
        }
        
        DatabaseManager.sharedManager.addOperation(getAll)
    }
    
    /**
    Retrieves all Tasks matching a given search term and status filter.
    
    - parameter field:      The field on which the search should be executed.
    - parameter searchTerm: The search term to match the field against.
    - parameter filter:     The status filter to apply to the query.
    - parameter completion: A completion block called upon operation completion.
    - note: The completion block is called on the main thread.
    */
    class func getTasks(field: String, searchTerm: String, filter: TaskStatus?, completion: ((tasks: [Task]) -> (Void))?) {
        var arguments = [AnyObject]()
        
        var sql = "SELECT * FROM (SELECT tasks.id as task_id, employee_id, title, description, status, estimated_completion_time, tasks.creation_date as task_creation_date, employees.id, username, first_name, last_name, email, password, role, employees.creation_date FROM tasks LEFT JOIN employees ON tasks.employee_id = employees.id) WHERE \(field) LIKE ?"
        
        arguments.append(searchTerm)
        
        if filter != nil {
            let filterExpression = " AND status = ?"
            sql = sql.stringByAppendingString(filterExpression)
            arguments.append(filter!.rawValue)
        }
        
        let searchQuery = QueryOperation(sql: sql, arguments: arguments) { (result, error) -> Void in
            var tasks = [Task]()
            
            while(result.next()) {
                tasks.append(Task(result: result))
            }
            
            print("Retrieved \(tasks.count) tasks.")
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let completion = completion {
                    completion(tasks: tasks)
                }
            })
        }
        
        DatabaseManager.sharedManager.addOperation(searchQuery)

    }
    
    /**
    Checks whether a Task matching the receiver's title property exists in the Db.
    
    - parameter completion: A completion block called upon operation completion.
    - note: The completion block is called on the main thread.
    */
    func exists(completion: ((success: Bool) -> (Void))?) {
        let count = QueryOperation(sql: "SELECT COUNT(*) FROM tasks where title = ?", arguments: [title]) { (result, error) -> Void in
            var total: Int = 0
            
            if result.next() {
                total = Int(result.intForColumnIndex(0))
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let completion = completion {
                    completion(success: total > 0)
                }
            })
        }
        
        DatabaseManager.sharedManager.addOperation(count)
    }
    
    /**
    Inserts the receiver into Db.
    
    - parameter completion: A completion block called upon operation completion.
    - note: The completion block is called on the main thread.
    */
    func insert(completion: ((success: Bool) -> Void)?) {
        let insertion = UpdateOperation(sql: TaskSQLConstants.TaskInsert, arguments: [title, employeeId, taskDescription, status.rawValue, estimatedCompletionTime]) { (success, error) -> Void in
            
            if let completion = completion {
                completion(success: success)
            }
        }
        
        DatabaseManager.sharedManager.addOperation(insertion)
    }
    
    /**
    Updates a Task matching the receiver with the receiver's data.
    
    - parameter updatedTask: The Task instance containing data to which the receiver should be updated.
    - parameter completion: A completion block called upon operation completion.
    - note: The completion block is called on the main thread.
    */
    func update(updatedTask: Task, completion: ((success: Bool) -> Void)?) {
        let update = UpdateOperation(sql: "UPDATE tasks SET \(TaskSQLConstants.TaskColumnNameTitle) = ?, \(TaskSQLConstants.TaskColumnNameDescription) = ?, \(TaskSQLConstants.TaskColumnNameStatus) = ?, \(TaskSQLConstants.TaskColumnNameEmployeeId) = ?, \(TaskSQLConstants.TaskColumnNameEstimatedCompletionTime) = ? WHERE id = ?", arguments: [updatedTask.title, updatedTask.taskDescription, updatedTask.status.rawValue, updatedTask.employeeId, updatedTask.estimatedCompletionTime, updatedTask.taskId]) { (success, error) -> Void in
            
            if let completion = completion {
                completion(success: success)
            }
        }
        
        DatabaseManager.sharedManager.addOperation(update)
    }
    
    /**
    Deletes the receiver from the Db.
    
    - parameter completion: A completion block called upon operation completion.
    - note: The completion block is called on the main thread. This method does not check whether the receiver exist in the Db, do that before hand using "exists".
    */
    func remove(completion: ((success: Bool) -> Void)?) {
        let deletion = UpdateOperation(sql: "DELETE FROM tasks WHERE id = ?", arguments: [Int(self.taskId)]) { (success, error) -> Void in
            
            if let completion = completion {
                completion(success: success)
            }
        }
        
        DatabaseManager.sharedManager.addOperation(deletion)
    }
    
    // MARK: -
    // MARK: Helpers
    // MARK: -
    
    /**
    Creates a copy of the receiver.
    
    - returns: Returns a newly initialized Task instance with data copied from the receiver.
    - note: A custom copy method was created because issues arose with the copyWithZone override.
    */
    func taskCopy() -> Task {
        let copy = Task(id: self.taskId, employeeId: self.employeeId, title: self.title, taskDescription: self.taskDescription, status: self.status, estimateCompletionTime: self.estimatedCompletionTime, creationDate: self.creationDate)
        copy.employee = self.employee?.employeeCopy()
        
        return copy
    }
    
    /**
    Compares the receiver to another Task instance.
    
    - parameter otherTask: A Task instance to compare the receiver to.
    
    - returns: Returns true if the two Task instances are equal, otherwise returns false.
    */
    func isEqualToTask(otherTask: Task?) -> Bool {
        
        if otherTask != nil {
            guard self.taskId == otherTask?.taskId else {
                print("The \"taskId\" is different.")
                return false
            }
            
            guard self.taskDescription == otherTask?.taskDescription else {
                print("The \"taskDescription\" is different.")
                return false
            }
            
            guard self.title == otherTask?.title else {
                print("The \"title\" is different.")
                return false
            }
            
            guard self.employeeId == otherTask?.employeeId else {
                print("The \"employeeId\" is different.")
                return false
            }
            
            guard self.status == otherTask?.status else {
                print("The \"status\" is different.")
                return false
            }
            
            guard self.estimatedCompletionTime == otherTask?.estimatedCompletionTime else {
                print("The \"status\" is different.")
                return false
            }
            
            return true
        } else {
            return false
        }
    }
    
    /**
    Overwrites the receiver's data with data from another Task instance.
    
    - parameter task: A Task instance with whos data to overwrite the receiver's data.
    */
    func updateTask(task: Task) {
        self.employeeId = task.employeeId
        self.employee = task.employee
        self.taskId = task.taskId
        self.title = task.title
        self.taskDescription = task.taskDescription
        self.status = task.status
        self.estimatedCompletionTime = task.estimatedCompletionTime
        self.creationDate = task.creationDate
    }
    
    
    /**
    Inspects the Task properties and determines whether the Task instance is valid for insertion into the Db.
    
    - returns: Returns true if valid, otherwise returns false.
    */
    func isValidForInsertion() -> Bool {
        guard !self.title.isEmpty else {
            return false
        }
        
        guard self.title != "Title not specified" else {
            return false
        }
        
        guard self.taskDescription != "Task description not specified" else {
            return false
        }
        
        guard !self.taskDescription.isEmpty else {
            return false
        }
        
        return true
    }
}
