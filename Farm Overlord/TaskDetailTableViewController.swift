//
//  TaskDetailTableViewController.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 11/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

protocol TaskDetailDelegate {
    func didUpdateTask(updatedTask: Task)
    func didAddTask(task: Task)
}

enum TaskDetailMode : String {
    case New = "New"
    case Edit = "Edit"
}

class TaskDetailTableViewController: UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, EmployeePickerDelegate, TextEditingDelegate {
    
    weak var task: Task?
    weak var textfield: UITextField?
    
    var delegate: TaskDetailDelegate?
    
    var editingTask: Task?
    var taskDetailMode: TaskDetailMode!
    
    var pickerOpen: Bool = false
    var sectionTitles = [String]()
    var taskStrings = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Task details"
        self.navigationItem.leftItemsSupplementBackButton = true
        self.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
        
        self.editingTask = self.taskDetailMode == TaskDetailMode.Edit ? self.task!.taskCopy() : Task()
        self.sectionTitles = ["Task details", "Task status", "Task handler"]
        
        self.updateTaskStrings()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateGUI()
    }
    
    // MARK: -
    // MARK: UIPickerViewDataSource
    // MARK: -
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch(row) {
        case 0:
            return "To do"
        case 1:
            return "In progress"
        case 2:
            return "Completed"
        default:
            return "N/A"
        }
    }
    
    // MARK: -
    // MARK: UIPickerViewDelegate
    // MARK: -
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let taskCell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 1)) as! TaskStatusTableViewCell
        let status = TaskStatus(rawValue: row)!
        
        self.editingTask!.status = status
        
        taskCell.setupCellWithTask(self.editingTask!, role: AppState.sharedInstance.signedInUser!.role, delegate: self)
        
        self.pickerOpen = !self.pickerOpen
        self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: 1, inSection: 1)], withRowAnimation: UITableViewRowAnimation.Automatic)
        self.updateGUI()
    }
    
    // MARK: -
    // MARK: UITableViewDataSource
    // MARK: -
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.sectionTitles.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2 {
            return 1
        } else {
            return 2
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell?
        
        switch(indexPath.section) {
        case 0:
            let taskCell = tableView.dequeueReusableCellWithIdentifier("textCell", forIndexPath: indexPath) as! TaskTextTableViewCell
            taskCell.setupCellWithText(self.taskStrings[indexPath.row], role: AppState.sharedInstance.signedInUser!.role)
            cell = taskCell
            break
        case 1:
            if indexPath.row == 0 {
                let taskCell = tableView.dequeueReusableCellWithIdentifier("statusCell", forIndexPath: indexPath) as! TaskStatusTableViewCell
                taskCell.setupCellWithTask(self.editingTask, role: AppState.sharedInstance.signedInUser!.role, delegate: self)
                cell = taskCell
            } else {
                let taskCell = tableView.dequeueReusableCellWithIdentifier("pickerCell", forIndexPath: indexPath) as! TaskStatusPickerTableViewCell
                taskCell.setupCellWithDelegate(self, delegate: self)
                
                taskCell.statusPicker.selectRow(self.editingTask!.status.rawValue, inComponent: 0, animated: false)
                
                taskCell.constraintHeight.constant = self.pickerOpen ? 100.0 : 0.0
                
                taskCell.setNeedsLayout()
                taskCell.layoutIfNeeded()
                
                cell = taskCell
            }
            break
        default:
            let taskCell = tableView.dequeueReusableCellWithIdentifier("textCell", forIndexPath: indexPath) as! TaskTextTableViewCell
            
            let username = self.editingTask?.employee?.username
            
            if username == nil {
                taskCell.setupCellWithText("Unassigned", role: AppState.sharedInstance.signedInUser!.role)
            } else {
                taskCell.setupCellWithText(self.editingTask!.employee!.username, role: AppState.sharedInstance.signedInUser!.role)
            }
            
            cell = taskCell
            break
        }
        
        return cell!
    }
    
    // MARK: -
    // MARK: UITableViewDelegate
    // MARK: -
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 && indexPath.section == 1 {
            if(AppState.sharedInstance.signedInUser!.role != EmployeeRole.Admin && AppState.sharedInstance.signedInUser!.employeeId != self.editingTask!.employeeId) {
                return
            }
            
            self.pickerOpen = !self.pickerOpen
            self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: 1, inSection: 1)], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
        
        if indexPath.section == 0 {
            if self.shouldPerformSegueWithIdentifier("segueTextEditing", sender: nil) {
                self.performSegueWithIdentifier("segueTextEditing", sender: indexPath.row == 0 ? "title" : "taskDescription")
            }
        }
        
        if indexPath.section == self.numberOfSectionsInTableView(self.tableView) - 1 {
            if self.shouldPerformSegueWithIdentifier("segueEmployeePicker", sender: nil) {
                self.performSegueWithIdentifier("segueEmployeePicker", sender: self)
            }
        }
        
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60.0
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sectionTitles[section]
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if self.textfield != nil {
            self.textfield!.resignFirstResponder()
        }
    }
    
    // MARK: -
    // MARK: UITextFieldDelegate
    // MARK: -
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if (range.length + range.location > textField.text?.characters.count) {
            return false;
        }
        
        let newLength = textField.text!.characters.count + string.characters.count - range.length
        return newLength <= 5
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.textfield = textField
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.validateEstimatedCompletionTime(textfield!)
    }
    
    // MARK: -
    // MARK: EmployeePickerDelegate
    // MARK: -
    
    func employeePicker(employeePicker: EmployeePickerTableViewController, didSelectEmployee employee: Employee) {
        self.editingTask?.employee = employee
        self.tableView.reloadData()
        
        self.updateGUI()
        
        self.navigationController?.popToViewController(self, animated: true)
    }
    
    // MARK: -
    // MARK: TextEditingDelegate
    // MARK: -
    
    func textEditor(textEditor: TextEditingTableViewController, didFinishEditing property: String, text: String) {
        self.editingTask?.setValue(text, forKey: property)
        self.updateTaskStrings()
        
        self.tableView.reloadData()
        self.updateGUI()
    }
    
    // MARK: -
    // MARK: Navigation
    // MARK: -
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if AppState.sharedInstance.signedInUser?.role != EmployeeRole.Admin {
            return false
        } else {
            return true
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueEmployeePicker" {
            let employeePickerTableViewController = segue.destinationViewController as! EmployeePickerTableViewController
            
            employeePickerTableViewController.delegate = self
            employeePickerTableViewController.currentlySelectedEmployee = self.editingTask?.employee
        } else if segue.identifier == "segueTextEditing" {
            let textEditingTableViewController = segue.destinationViewController as! TextEditingTableViewController
            
            textEditingTableViewController.delegate = self
            textEditingTableViewController.property = sender as? String
            textEditingTableViewController.headerTitle = textEditingTableViewController.property == "title" ? "Task title" : "Task description"
            textEditingTableViewController.allowEmptyText = false
            textEditingTableViewController.text = self.editingTask!.valueForKey(textEditingTableViewController.property!) as? String
        }
    }
    
    // MARK: -
    // MARK: Actions
    // MARK: -
    
    func buttonSaveTapped(sender: UIBarButtonItem) {
        self.task?.update(self.editingTask!) { (success) -> Void in
            if success {
                self.delegate?.didUpdateTask(self.editingTask!)
                
                self.updateTask()
                self.updateGUI()
            } else {
                Utilites.showAlertController("Task update", message: "Failed updating the Task information.", style: UIAlertControllerStyle.Alert, actions: [], presenter: self)
            }
            
        }
    }
    
    func buttonAddTapped(sender: UIBarButtonItem) {
        if self.editingTask!.isValidForInsertion() {
            self.editingTask!.exists({ (success) -> (Void) in
                if success {
                    Utilites.showAlertController("Task creation", message: "A Task with the specified title already exists. Please correct the issue and try again.", style: UIAlertControllerStyle.Alert, actions: [], presenter: self)
                } else {
                    self.editingTask!.insert({ (success) -> Void in
                        if success {
                            self.delegate?.didAddTask(self.editingTask!)
                            self.navigationItem.setRightBarButtonItem(nil, animated: true)
                        } else {
                            Utilites.showAlertController("Task creation", message: "Failed creating the new Task. Please check the information you've entered and try again.", style: UIAlertControllerStyle.Alert, actions: [], presenter: self)
                        }
                    })
                }
            })
        }
    }
    
    // MARK: -
    // MARK: Helpers
    // MARK: -
    
    func updateGUI() {
        if self.taskDetailMode == TaskDetailMode.New {
            if self.isValid() {
                self.navigationItem.setRightBarButtonItem(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: Selector("buttonAddTapped:")), animated: true)
            } else {
                self.navigationItem.setRightBarButtonItem(nil, animated: true)
            }
        } else {
            if self.isChanged() {
                self.navigationItem.setRightBarButtonItem(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Save, target: self, action: Selector("buttonSaveTapped:")), animated: true)
            } else {
                self.navigationItem.setRightBarButtonItem(nil, animated: true)
            }
        }
    }
    
    func updateTask() {
        self.task!.employeeId = self.editingTask!.employeeId
        self.task!.employee = self.editingTask!.employee
        self.task!.taskId = self.editingTask!.taskId
        self.task!.title = self.editingTask!.title
        self.task!.taskDescription = self.editingTask!.taskDescription
        self.task!.status = self.editingTask!.status
        self.task!.estimatedCompletionTime = self.editingTask!.estimatedCompletionTime
        self.task!.creationDate = self.editingTask!.creationDate
    }
    
    func updateTaskStrings() {
        self.taskStrings = [self.editingTask!.title, self.editingTask!.taskDescription]
    }
    
    /**
    Determines whether the edited Task has been changed, thus showing the Save button in the navigation bar.
    
    - returns: Returns true if a change to the edited Task has occured, otherwise returns false.
    */
    func isChanged() -> Bool {
        return !self.editingTask!.isEqualToTask(self.task)
    }
    
    /**
    Determines whether the newly created Task is valid for insertion, thus showing the Done button in the navigation bar.
    
    - returns: Returns true if the Task is valid for insertion, otherwise returns false.
    */
    func isValid() -> Bool {
        return self.editingTask!.isValidForInsertion()
    }
    
    /**
    Validates the estimated completion time textfield.
    
    - parameter textfield: The textfiedl which text needs validation.
    */
    func validateEstimatedCompletionTime(textfield: UITextField) {
        let number: Double? = Double(textfield.text!)
        
        if number == nil {
            Utilites.showAlertController("Update", message: "The estimated completion time must be a number.", style: UIAlertControllerStyle.Alert, actions: [], presenter: self)
            textfield.text = NSNumberFormatter().stringFromNumber(self.editingTask!.estimatedCompletionTime)
        } else {
            self.editingTask!.estimatedCompletionTime = Double(number!)
            self.updateGUI()
        }
    }
    
    deinit {
        print("Deallocating the TaskDetailTableViewController.")
    }
}
