//
//  LoginViewController.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 02/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    /// The width of the content view hosting the form within the ScrollView.
    @IBOutlet weak var constraintWidth: NSLayoutConstraint!
    
    /// The height of the content view hosting the form within the ScrollView.
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var activeTextField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Add the observes for the the keyboard show/hide notifications - used to scroll a text field if it's being obscured by the keyboard.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardDidShow:"), name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardDidHide:"), name: UIKeyboardDidHideNotification, object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // Make the username text field active.
        self.usernameTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Remove the observers so that self doesn't listen to notifications produced by the keyboard appearing on other controllers.
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidHideNotification, object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        // Update the content view size, necessary to properly size the view upon orientation change.
        self.constraintWidth.constant = CGRectGetWidth(self.view.bounds)
        self.constraintHeight.constant = CGRectGetHeight(self.view.bounds)
    }
    
    // MARK: -
    // MARK: UITextFieldDelegate
    // MARK: -
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.activeTextField = textField
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.activeTextField = nil
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // Tapping the return key on the username textfield will make the password texfield active,
        // tapping the return key on the password textfield will initiate a sign in attempt with the data entered.
        if textField.isEqual(usernameTextField) {
            self.passwordTextField.becomeFirstResponder()
        } else {
            self.passwordTextField.resignFirstResponder()
            self.signIn()
        }
        
        return true
    }
    
    // MARK: -
    // MARK: Keyboard
    // MARK: -
    
    /**
    Scrolls the active texfield into view if it's being obscured by the keyboard.
    
    - parameter notification: The NSNotification instance for the keyboardDidShow notification.
    */
    func keyboardDidShow(notification: NSNotification) {
        let keyboardInfo = notification.userInfo!
        
        var kbRect: CGRect = (keyboardInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        kbRect = self.view.convertRect(kbRect, fromView: nil)
        
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, CGRectGetHeight(kbRect), 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var viewRect = self.view.frame
        viewRect.size.height -= CGRectGetHeight(kbRect)
        
        if !CGRectContainsPoint(viewRect, self.activeTextField!.frame.origin) {
            self.scrollView.scrollRectToVisible(self.activeTextField!.frame, animated: true)
        }
    }
    
    /**
    Resets the scroll view when the keyboard disappears.
    
    - parameter notification: The NSNotification instance for the keyboardDidHide notification.
    */
    func keyboardDidHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsetsZero
        
        UIView.animateWithDuration(0.25) { () -> Void in
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
        }
    }
    
    // MARK: -
    // MARK: Helpers
    // MARK: -
    
    func signIn() {
        // <---------------- UNCOMMENT TO PROGRAMATICALLY INSERT A NEW ADMIN EMPLOYEE INTO THE DATABASE) ---------------->
        let insertionEmployee = Employee(employeeId: 0, username: "D6mi", firstName: "Domagoj", lastName: "Kulundžić", email: "domagoj1989@gmail.com", password: "domidomi", role: EmployeeRole.Admin, creationDate: nil)
        insertionEmployee.insert(nil)
        
        // <---------------- UNCOMMENT TO PROGRAMATICALLY INSERT TASKS INTO THE DATABASE (COMMENT THIS AFTER RUNNING IT ONCE) ---------------->
        
//        var task = Task(id: 0, employeeId: 1, title: "Milk the cow", taskDescription: "The milk should be gathered from the Moo-moo.", status: TaskStatus.ToDo, estimateCompletionTime: 100, creationDate: nil)
//        
//        task.insert(nil)
//        
//        task = Task(id: 0, employeeId: 1, title: "Feed the chickens", taskDescription: "The chickens should be fed.", status: TaskStatus.InProgress, estimateCompletionTime: 25, creationDate: nil)
//        task.insert(nil)
//    
//        task = Task(id: 0, employeeId: 2, title: "Slaughter the pigs", taskDescription: "The pigs should be harvested for meat and all other goodies.", status: TaskStatus.ToDo, estimateCompletionTime: 200, creationDate: nil)
//        task.insert(nil)
//        
//        task = Task(id: 0, employeeId: 2, title: "Release the horses for a run", taskDescription: "The horses should be released so they can run free in the wild - for a while.", status: TaskStatus.Completed, estimateCompletionTime: 125, creationDate: nil)
//        task.insert(nil)
//        
//        task = Task(id: 0, employeeId: 3, title: "Hot time on the hay", taskDescription: "The humanly pleasures should be consumated on a soft, haystack.", status: TaskStatus.InProgress, estimateCompletionTime: 80, creationDate: nil)
//        task.insert(nil)
        
        if validate() {
            let tempEmployee = Employee(employeeId: 0, username: self.usernameTextField.text!, firstName: "Domagoj", lastName: "Kulundžić", email: "domagoj1989@gmail.com", password: self.passwordTextField.text!, role: EmployeeRole.Admin, creationDate: nil)
            
            tempEmployee.signIn({ (employee) -> (Void) in
                
                guard employee != nil else {
                    Utilites.showAlertController("Login", message: "A user with the specified username/password combination does not exist. Please contact your administrator for assistance.", style: UIAlertControllerStyle.Alert, actions: [UIAlertAction](), presenter: self)
                    return                    
                }
                
                AppState.sharedInstance.signedInUser = employee
                
                self.openMain()
            })
        }
    }
    
    /**
    Validates the form.
    
    - returns: Returns true if the form entries are valid, otherwise returns false.
    */
    func validate() -> Bool {
        var isValidated = true
        var message: String?
        
        if self.usernameTextField.text == nil {
            message = "The username field is mandatory."
            isValidated = false
        }
        
        if self.passwordTextField.text != nil {
            if self.passwordTextField.text!.isEmpty {
                message = "The password field is mandatory."
                return false
            }
        }
        
        if !isValidated  {
            Utilites.showAlertController("Login", message: message!, style: UIAlertControllerStyle.Alert, actions: [UIAlertAction](), presenter: self)
        }
        
        return isValidated
    }
    
    /**
    Opens the main part of the app. The way it opens depends on the whether the LoginViewController's been presented modally or not.
    - note: If the LoginViewController's been presented modally, for instance after a logout, the LoginViewController will just 
    be dismissed and control will return to the SplitViewController. If the LoginViewController's not presented modally, meaning the app
    started with the LoginViewController as initial, the segueSplit will executed, showing the SplitViewController.
    */
    func openMain() {
        if self.presentingViewController != nil {
            self.dismissViewControllerAnimated(true, completion: nil)
        } else {
            self.performSegueWithIdentifier("segueSplit", sender: self)
        }
    }
}
