//
//  EmployeeTableViewCell.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 13/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

class EmployeeTableViewCell: UITableViewCell {
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
    func setupCellWithEmployee(employee: Employee) {
        self.labelUsername.text = employee.username
        self.labelName.text = employee.name
    }
}
