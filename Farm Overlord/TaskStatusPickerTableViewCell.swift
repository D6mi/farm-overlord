//
//  StatusPickerTableViewCell.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 12/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

class TaskStatusPickerTableViewCell: UITableViewCell {
    @IBOutlet weak var statusPicker: UIPickerView!
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
    
    func setupCellWithDelegate(dataSource: UIPickerViewDataSource, delegate: UIPickerViewDelegate) {
        self.statusPicker.dataSource = dataSource
        self.statusPicker.delegate = delegate
    }
}
