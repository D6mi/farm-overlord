//
//  EmployeeManagementViewController.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 06/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

class EmployeeManagementViewController: UIViewController, EmployeeDetailDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var employees = [Employee]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setRightBarButtonItem(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: Selector("addEmployee:")), animated: false)
        self.reload()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.tableView.indexPathForSelectedRow != nil {
            self.tableView.deselectRowAtIndexPath(self.tableView.indexPathForSelectedRow!, animated: true)
        }
    }
    
    // MARK: -
    // MARK: UITableViewDataSource
    // MARK: -
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employees.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("employeeCell", forIndexPath: indexPath) as! EmployeeTableViewCell
        
        let employee = employees[indexPath.row]
        cell.setupCellWithEmployee(employee)
        
        return cell
    }
    
    // MARK: -
    // MARK: UITableViewDelegate
    // MARK: -
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) -> Void {
        let employee = self.employees[indexPath.row]
        self.openDetail(employee)
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Employee management"
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // MARK: -
    // MARK: EmployeeDetailDelegate
    // MARK: -
    
    func didDeleteEmployee(employee: Employee) {
        if let index = self.employees.indexOf(employee) {
            self.employees.removeAtIndex(index)
            self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Middle)
            
            if self.employees.count > 0 {
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                
                self.tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
                
                if self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular {
                    self.openDetail(self.employees[indexPath.row])
                } else {
                    self.navigationController?.popViewControllerAnimated(true)
                }
            } else {
                self.openDetail(nil)
            }
        }
    }
    
    func didUpdateEmployee(updatedEmployee: Employee) {
        var index: Int = 0
        
        for employee in self.employees {
            if employee.employeeId == updatedEmployee.employeeId {
                index = self.employees.indexOf(employee)!
            }
        }
        
        self.employees[index] = updatedEmployee
        
        let indexPathToReload = NSIndexPath(forRow: index, inSection: 0)
        self.tableView.reloadRowsAtIndexPaths([indexPathToReload], withRowAnimation: UITableViewRowAnimation.Automatic)
        
    }
    
    func didAddEmployee(employee: Employee) {
        self.reload()
        
        if self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular {
            self.openDetail(nil)
        } else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    // MARK: -
    // MARK: Actions
    // MARK: -
    
    func openDetail(employee: Employee?) {
        let detailController = self.storyboard!.instantiateViewControllerWithIdentifier("employeeDetailTableViewController") as? EmployeeDetailTableViewController
        detailController!.employee = employee
        detailController!.delegate = self
        
        let navigationController = UINavigationController(rootViewController: detailController!)
        self.showDetailViewController(navigationController, sender: self)
    }
    
    func addEmployee(sender: UIBarButtonItem) {
        self.openDetail(nil)
    }
    
    func reload() {
        self.activityIndicator.startAnimating()
        
        Employee.getAll { (employees) -> (Void) in
            self.activityIndicator.stopAnimating()
            
            self.employees = employees
            self.tableView.reloadData()
        }
    }
}
