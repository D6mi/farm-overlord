//
//  TaskGroup.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 11/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

/// A single TaskGroup represents a single section on the TasksViewController by grouping the related Employee's title with tasks assigned to that Employee.
class TaskGroup: NSObject {
    var title: String
    var tasks = [Task]()
    
    /**
    Initializes a new instance with the specified parameters.
    
    - parameter title: The username of the Employee to whom this TaskGroup instance relates to.
    - parameter tasks: A collection of Task instances assigned to the TaskGroup employee.
    
    - returns: Returns a newly initialized TaskGroup instance.
    - note: The tasks within each TaskGroup are sorted by creation date, but the creation date's currently retrieved from the Db in incorrect form, known bug.
    */
    init(title: String, tasks: [Task]) {
        self.title = title
        self.tasks = tasks
        
        self.tasks = self.tasks.sort { (task1, task2) -> Bool in
            let result = task1.creationDate!.compare(task2.creationDate!)
            
            if result == NSComparisonResult.OrderedDescending {
                return true
            } else {
                return false
            }
        }
    }
}
