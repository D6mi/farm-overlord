//
//  DatabaseOperation.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 05/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

///  A base class encapsulating data need to perform a database fetch request on a background thread.
class DatabaseOperation: NSOperation {
    var sql: String
    var arguments: [AnyObject]?
    
    init(sql: String, arguments: [AnyObject]?) {
        self.sql = sql
        self.arguments = arguments
    }
}
