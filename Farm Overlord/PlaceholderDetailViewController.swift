//
//  PlaceholderDetailViewController.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 07/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

/// A UIViewController that acts as a placeholder detail view controller for the SplitViewController where masters do not have a matching 
/// detail view controller.
class PlaceholderDetailViewController: UIViewController {
    @IBOutlet weak var labelInstruction: UILabel!
    @IBOutlet weak var imageLeftUp: UIImageView!
    @IBOutlet weak var imageLeftDown: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.labelInstruction.textColor = self.view.tintColor
        
        self.imageLeftUp.image = self.imageLeftUp.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        self.imageLeftUp.tintColor = self.view.tintColor
        
        self.imageLeftDown.image = self.imageLeftDown.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        self.imageLeftDown.tintColor = self.view.tintColor
    }
}
