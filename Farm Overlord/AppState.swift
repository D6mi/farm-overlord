//
//  AppState.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 06/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

class AppState: NSObject {
    static let sharedInstance = AppState()
    
    struct Constants {
        static let EmployeeLocalStorageKey = "FarmOverlordEmployee"
    }
    
    var signedInUser: Employee? {
        didSet {
            if self.signedInUser == nil {
                NSUserDefaults.standardUserDefaults().removeObjectForKey(Constants.EmployeeLocalStorageKey)
                NSUserDefaults.standardUserDefaults().synchronize()
            } else {
                let userData = NSKeyedArchiver.archivedDataWithRootObject(self.signedInUser!)
                NSUserDefaults.standardUserDefaults().setObject(userData, forKey: Constants.EmployeeLocalStorageKey)
                NSUserDefaults.standardUserDefaults().synchronize()
            }
        }
    }
    
    override init() {
        super.init()
        
        let userData = NSUserDefaults.standardUserDefaults().objectForKey(Constants.EmployeeLocalStorageKey) as? NSData
        if userData != nil {
            self.signedInUser = NSKeyedUnarchiver.unarchiveObjectWithData(userData!) as? Employee
        }
        
    }
}
