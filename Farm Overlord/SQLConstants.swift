//
//  SQLConstants.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 01/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

struct SQLConstants {
    static let DROP_TABLE_TASKS = "DROP TABLE IF EXISTS tasks"
    static let DROP_TABLE_EMPLOYESS = "DROP TABLE IF EXISTS employees"
    
    static let CREATE_TABLE_TASKS = "CREATE TABLE IF NOT EXISTS tasks (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, employee_id INTEGER DEFAULT 0, title VARCHAR(32) UNIQUE NOT NULL, description VARCHAR(128) NOT NULL, status INTEGER NOT NULL DEFAULT 0, estimated_completion_time INTEGER, creation_date DATETIME DEFAULT (CURRENT_TIMESTAMP), FOREIGN KEY (employee_id) REFERENCES employees(id))"
    
    static let CREATE_TABLE_EMPLOYEES = "CREATE TABLE IF NOT EXISTS employees (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username VARCHAR(32) UNIQUE NOT NULL, first_name VARCHAR(32) NOT NULL, last_name VARCHAR(32) NOT NULL, email VARCHAR(320) UNIQUE, password VARCHAR(32) NOT NULL, role INTEGER NOT NULL, creation_date DATETIME DEFAULT (CURRENT_TIMESTAMP))"
}
