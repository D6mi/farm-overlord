//
//  Employee.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 01/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

enum EmployeeRole : Int {
    case Admin
    case User
}

public struct EmployeeSQLConstants {
    static let EmployeeColumnNameID = "id"
    static let EmployeeColumnNameUsername = "username"
    static let EmployeeColumnNameFirstName = "first_name"
    static let EmployeeColumnNameLastName = "last_name"
    static let EmployeeColumnNameEmail = "email"
    static let EmployeeColumnNamePassword = "password"
    static let EmployeeColumnNameRole = "role"
    static let EmployeeColumnNameCreationDate = "creation_date"
    
    static let EmployeeInsert = "INSERT INTO employees (username, first_name, last_name, email, password, role) VALUES  (?, ?, ?, ?, ?, ?)"
}

class Employee: NSObject, NSCoding {
    var employeeId: Int
    var username: String
    var firstName: String
    var lastName: String
    var email: String
    var password: String?
    var role: EmployeeRole
    var creationDate: NSDate
    
    var name: String {
        get {
            return self.firstName + " " + self.lastName
        }
    }
    
    init(employeeId: Int, username: String, firstName: String, lastName: String, email: String, password: String, role: EmployeeRole, creationDate: NSDate?) {
        self.employeeId = employeeId
        self.username = username
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.password = password
        self.role = role
        
        if let date = creationDate {
            self.creationDate = date as NSDate!
        } else {
            self.creationDate = NSDate()
        }
    }
    
    init(result: FMResultSet) {
        self.employeeId = Int(result.intForColumn(EmployeeSQLConstants.EmployeeColumnNameID))
        self.username = result.stringForColumn(EmployeeSQLConstants.EmployeeColumnNameUsername)
        self.firstName = result.stringForColumn(EmployeeSQLConstants.EmployeeColumnNameFirstName)
        self.lastName = result.stringForColumn(EmployeeSQLConstants.EmployeeColumnNameLastName)
        self.email = result.stringForColumn(EmployeeSQLConstants.EmployeeColumnNameEmail)
        self.password = result.stringForColumn(EmployeeSQLConstants.EmployeeColumnNamePassword)
        self.role = EmployeeRole(rawValue: Int(result.intForColumn(EmployeeSQLConstants.EmployeeColumnNameRole)))!
        self.creationDate = result.dateForColumn(EmployeeSQLConstants.EmployeeColumnNameCreationDate)
    }
    
    // MARK: -
    // MARK: Actions
    // MARK: -
    
    /**
    Signs in the receiver.
    
    - parameter completion: A completion block called upon operation completion.
    - note: The completion block is called on the main thread. The sign in method queries the Db for data validity and, after the Db validates the username and password, calls the completion block with the result.
    */
    func signIn(completion: ((employee: Employee?) -> (Void))?) {
        let count = QueryOperation(sql: "SELECT * FROM employees WHERE username = ? AND password = ?", arguments: [username, password!]) { (result, error) -> Void in
            
            var total: Int = 0
            var employee: Employee?
            
            if result.next() {
                total = Int(result.intForColumnIndex(0))
            }
            
            if total > 0 {
                employee = Employee(result: result)
            }
            
            if let completion = completion {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(employee: employee)
                })
            }
        }
        
        DatabaseManager.sharedManager.addOperation(count)
    }
    
    
    // MARK: -
    // MARK: Database
    // MARK: -
    
    /**
    Retrieves all Employees existing in the Db.
    
    - parameter completion: A completion block called upon operation completion.
    - note: The completion block is called on the main thread.
    */
    class func getAll(completion: ((employees: [Employee]) -> (Void))?) {
        let getAll = QueryOperation(sql: "SELECT * FROM employees", arguments: []) { (result, error) -> Void in
            var employees = [Employee]()
            
            while(result.next()) {
                let employee = Employee(result: result)
                employees.append(employee)
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let completion = completion {
                    completion(employees: employees)
                }
            })
        }
        
        DatabaseManager.sharedManager.addOperation(getAll)
    }
    
    /**
    Checks whether a Employee matching the receiver's username or email property exists in the Db.
    
    - parameter completion: A completion block called upon operation completion.
    - note: The completion block is called on the main thread.
    */
    func exists(completion: ((success: Bool) -> (Void))?) {
        let count = QueryOperation(sql: "SELECT COUNT(*) FROM employees where username = ? OR email = ?", arguments: [username, email]) { (result, error) -> Void in
            
            var total: Int = 0
            
            if result.next() {
                total = Int(result.intForColumnIndex(0))
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let completion = completion {
                    completion(success: total > 0)
                }
            })
        }
        
        DatabaseManager.sharedManager.addOperation(count)
    }
    
    /**
    Inserts the receiver into Db.
    
    - parameter completion: A completion block called upon operation completion.
    - note: The completion block is called on the main thread.
    */
    func insert(completion: ((success: Bool) -> Void)?) {
        let insertion = UpdateOperation(sql: EmployeeSQLConstants.EmployeeInsert, arguments: [username, firstName, lastName, email, password!, role.rawValue]) { (success, error) -> Void in
            
            if let completion = completion {
                completion(success: success)
            }
        }
        
        DatabaseManager.sharedManager.addOperation(insertion)
    }
    
    /**
    Updates a Employee matching the receiver with the receiver's data.
    
    - parameter updatedEmployee: The Employee instance containing data to which the receiver should be updated.
    - parameter completion: A completion block called upon operation completion.
    - note: The completion block is called on the main thread.
    */
    func update(updatedEmployee: Employee, completion: ((success: Bool) -> Void)?) {
        let update = UpdateOperation(sql: "UPDATE employees SET \(EmployeeSQLConstants.EmployeeColumnNameUsername) = ?, \(EmployeeSQLConstants.EmployeeColumnNameFirstName) = ?, \(EmployeeSQLConstants.EmployeeColumnNameLastName) = ?, \(EmployeeSQLConstants.EmployeeColumnNameEmail) = ?, \(EmployeeSQLConstants.EmployeeColumnNameRole) = ? WHERE id = ?", arguments: [updatedEmployee.username, updatedEmployee.firstName, updatedEmployee.lastName, updatedEmployee.email, updatedEmployee.role.rawValue, Int(updatedEmployee.employeeId)]) { (success, error) -> Void in
            
            if let completion = completion {
                completion(success: success)
            }
        }
        
        DatabaseManager.sharedManager.addOperation(update)
    }
    
    /**
    Deletes the receiver from the Db.
    
    - parameter completion: A completion block called upon operation completion.
    - note: The completion block is called on the main thread. This method does not check whether the receiver exist in the Db, do that before hand using "exists".
    */
    func remove(completion: ((success: Bool) -> Void)?) {
        let deletion = UpdateOperation(sql: "DELETE FROM employees WHERE id = ?", arguments: [Int(self.employeeId)]) { (success, error) -> Void in
            
            if let completion = completion {
                completion(success: success)
            }
        }
        
        DatabaseManager.sharedManager.addOperation(deletion)
    }
    
    // MARK: -
    // MARK: NSCoding
    // MARK: -
    
    required init?(coder aDecoder: NSCoder) {
        self.employeeId = Int(aDecoder.decodeIntForKey(EmployeeSQLConstants.EmployeeColumnNameID))
        self.username = aDecoder.decodeObjectForKey(EmployeeSQLConstants.EmployeeColumnNameUsername) as! String
        self.firstName = aDecoder.decodeObjectForKey(EmployeeSQLConstants.EmployeeColumnNameFirstName) as! String
        self.lastName = aDecoder.decodeObjectForKey(EmployeeSQLConstants.EmployeeColumnNameLastName) as! String
        self.email = aDecoder.decodeObjectForKey(EmployeeSQLConstants.EmployeeColumnNameEmail) as! String
        self.role = EmployeeRole(rawValue: Int(aDecoder.decodeIntForKey(EmployeeSQLConstants.EmployeeColumnNameRole)))!
        self.creationDate = aDecoder.decodeObjectForKey(EmployeeSQLConstants.EmployeeColumnNameCreationDate) as! NSDate
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeInteger(self.employeeId, forKey: EmployeeSQLConstants.EmployeeColumnNameID)
        aCoder.encodeObject(self.username, forKey: EmployeeSQLConstants.EmployeeColumnNameUsername)
        aCoder.encodeObject(self.firstName, forKey: EmployeeSQLConstants.EmployeeColumnNameFirstName)
        aCoder.encodeObject(self.lastName, forKey: EmployeeSQLConstants.EmployeeColumnNameLastName)
        aCoder.encodeObject(self.email, forKey: EmployeeSQLConstants.EmployeeColumnNameEmail)
        aCoder.encodeInteger(self.role.rawValue, forKey: EmployeeSQLConstants.EmployeeColumnNameRole)
        aCoder.encodeObject(self.creationDate, forKey: EmployeeSQLConstants.EmployeeColumnNameCreationDate)
    }
    
    // MARK: -
    // MARK: Helpers
    // MARK: -
    
    /**
    Creates a String from a EmployeeRole enum.
    
    - returns: Returns a String represantion of the a EmployeeRole enum.
    */
    func stringForRole() -> String {
        var roleString = ""
        
        switch(self.role) {
        case .User:
            roleString = "User"
            break;
        case .Admin:
            roleString = "Administrator"
            break;
        }
        
        return roleString
    }
    
    /**
    Creates a copy of the receiver.
    
    - returns: Returns a newly initialized Employee instance with data copied from the receiver.
    - note: A custom copy method was created because issues arose with the copyWithZone override.
    */
    func employeeCopy() -> Employee {
        let copy = Employee(employeeId: self.employeeId, username: self.username, firstName: self.firstName, lastName: self.lastName, email: self.email, password: self.password!, role: self.role, creationDate: self.creationDate)
        
        return copy
    }
    
    /**
    - returns: Returns all of the roles available.
    */
    class func roles() -> [String] {
        return ["Administrator", "User"]
    }
}
