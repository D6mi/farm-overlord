//
//  UpdateOperation.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 05/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

/**
A subclass of DatabaseOperation, represents a single update (every operation except SELECT) performed on the database.
- note: The class is only responsible for delivering the results to the caller, it DOES NOT call the passed in completion block on the main thread,
the caller's responsible for that.
*/
class UpdateOperation: DatabaseOperation {
    var completion: ((success: Bool, error: NSError?) -> Void)?
    
    /**
    Initializes a new instance.
    
    - parameter sql:        The SQL update statement to run against the Database.
    - parameter arguments:  Any array of objects that will be used as arguments to the update.
    - parameter completion: A completion block to be executed once the query completed.
    - parameter error:      A NSError instance describing the error, nil if no error occurred.
    
    - returns: Returns a newly initialized instance of the QueryOpereation class.
    */
    init(sql: String, arguments: [AnyObject]?, completion:((success: Bool, error: NSError?) -> Void)?) {
        self.completion = completion
    
        super.init(sql: sql, arguments: arguments)
    }
    
    override func main() {
        let queue = DatabaseManager.sharedManager.databaseQueue
        
        queue.inDatabase { (db) -> Void in
            let success = db.executeUpdate(self.sql, withArgumentsInArray: self.arguments)
            db.traceExecution = true
            
            if let completion = self.completion {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(success: success, error: nil)
                })
            }
        }        
    }
}
