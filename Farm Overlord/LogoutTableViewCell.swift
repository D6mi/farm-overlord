//
//  LogoutTableViewCell.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 14/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

class LogoutTableViewCell: UITableViewCell {
    @IBOutlet weak var buttonLogout: UIButton!
    
    func setupCellWithTarget(target: AnyObject, action: Selector, controlEvents: UIControlEvents) {
        self.buttonLogout.addTarget(target, action: action, forControlEvents: controlEvents)
    }
}
