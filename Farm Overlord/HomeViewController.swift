//
//  HomeViewController.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 06/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISplitViewControllerDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    var actionGroups: [ActionGroup]!
    var placeholderDetailViewController: PlaceholderDetailViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.splitViewController!.delegate = self
        
        self.navigationItem.title = "Farm overlord"
        
        self.actionGroups = Utilites.actionGroupsForRole(AppState.sharedInstance.signedInUser!.role)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.actionGroups = Utilites.actionGroupsForRole(AppState.sharedInstance.signedInUser!.role)
        self.tableView.reloadData()
        
        if self.splitViewController?.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular {
            if self.placeholderDetailViewController == nil {
                self.placeholderDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("placeholderDetailViewController") as? PlaceholderDetailViewController
            }
            
            self.showDetailViewController(self.placeholderDetailViewController!, sender: self)
        }
    }
    
    func splitViewController(svc: UISplitViewController, willChangeToDisplayMode displayMode: UISplitViewControllerDisplayMode) {
        print(displayMode)
    }
    
    
    // MARK: -
    // MARK: UITableViewDataSource
    // MARK: -
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return actionGroups.count + 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == actionGroups.count ? 1 : actionGroups[section].actions.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == actionGroups.count {
            let cell = tableView.dequeueReusableCellWithIdentifier("logoutCell", forIndexPath: indexPath) as! LogoutTableViewCell
            
            cell.setupCellWithTarget(self, action: Selector("buttonLogoutTapped:"), controlEvents: UIControlEvents.TouchUpInside)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("actionCell", forIndexPath: indexPath)
            
            let action = actionGroups[indexPath.section].actions[indexPath.row]
            cell.textLabel?.text = action.title
            
            return cell
        }
    }
    
    // MARK: -
    // MARK: UITableViewDelegate
    // MARK: -
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == self.actionGroups.count {
            return
        }
        
        let action = actionGroups[indexPath.section].actions[indexPath.row]
        
        if action.controllerId == "employeeManagementViewController" {
            self.performSegueWithIdentifier("segueEmployeeManagement", sender: self)
        } else if action.controllerId == "tasksViewController" {
            self.performSegueWithIdentifier("segueTasks", sender: self)
        }
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section < self.actionGroups.count {
            let actionGroup = actionGroups[section]
            return actionGroup.title
        } else {
            return ""
        }
    }
    
    func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == self.numberOfSectionsInTableView(tableView) - 1 {
            return "Signed in as \(AppState.sharedInstance.signedInUser!.username)"
        } else {
            return ""
        }
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // MARK: -
    // MARK: Actions
    // MARK: -
    
    func buttonLogoutTapped(sender: UIButton) {
        let confirmAction = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.Destructive) { (action) -> Void in
            self.logoutUser()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (action) -> Void in
        }
        
        Utilites.showAlertController("Logout", message: "Are you sure you want to logout?", style: UIAlertControllerStyle.Alert, actions: [cancelAction, confirmAction], presenter: self)
    }
    
    // MARK: -
    // MARK: Helpers
    // MARK: -
    
    func logoutUser() {
        AppState.sharedInstance.signedInUser = nil
        
        if self.splitViewController?.presentingViewController == nil {
            let loginViewController = self.storyboard?.instantiateViewControllerWithIdentifier("loginViewController") as! LoginViewController
            self.presentViewController(loginViewController, animated: true, completion: nil)
        } else {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}
