//
//  TextEditingTableViewController.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 12/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

protocol TextEditingDelegate {
    /**
    Called on the delegate instance once the user has tapped Done.
    
    - parameter textEditor: A instance of the TextEditingTableViewController from which the delegate call originates from.
    - parameter property:   The property that was edited.
    - parameter text:       The text that was entered.
    */
    func textEditor(textEditor: TextEditingTableViewController, didFinishEditing property: String, text: String)
}

class TextEditingTableViewController: UITableViewController, UITextViewDelegate {
    @IBOutlet weak var textView: UITextView!
    
    var text: String?
    var property: String?
    var headerTitle: String?
    var allowEmptyText: Bool = false
    
    var previousRect = CGRectZero
    
    var delegate: TextEditingDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textView.text = self.text!
        self.textView.scrollsToTop = false
        
        self.tableView.reloadData()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: Selector("buttonDoneTapped:"))
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Update the textView size (the contentSize property is not fully set in viewDidLoad)
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.textView.becomeFirstResponder()
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.textView.contentSize.height * 1.5
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.headerTitle != nil ? self.headerTitle : "Editing"
    }
    
    override func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return self.allowEmptyText ? "Empty text is allowed." : "Empty text is not allowed, please enter a fully qualified \(self.headerTitle!)."
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        return textView.text.characters.count + (text.characters.count - range.length) <= 128;
    }
    
    func textViewDidChange(textView: UITextView) {
        // Track the caret position so we can determine whether a line break occured.
        let textPosition = textView.endOfDocument
        let currentRect = textView.caretRectForPosition(textPosition)
        
        // Refresh the table view height if a line break occured.
        if currentRect.origin.y > self.previousRect.origin.y {
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
        
        self.previousRect = currentRect
        
        // If empty text is allowed, ignore the next code that checks text validity.
        if self.allowEmptyText {
            return
        }
        
        let text = textView.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    
        // Disable the Done button if the no text has been entered.
        if text.isEmpty {
            self.navigationItem.rightBarButtonItem?.enabled = false
        } else {
            self.navigationItem.rightBarButtonItem?.enabled = true
        }
    }
    
    func buttonDoneTapped(sender: UIBarButtonItem) {
        if delegate != nil {
            let text = self.textView.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            delegate!.textEditor(self, didFinishEditing: self.property!, text: text)
        }
        
        self.navigationController?.popViewControllerAnimated(true)
    }
}
