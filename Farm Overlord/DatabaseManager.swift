//
//  DatabaseManager.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 01/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

class DatabaseManager: NSObject {
    let DATABASE_RESOURCE_NAME = "farm_overlord"
    let DATABASE_RESOURCE_TYPE = "sqlite"
    let DATABASE_FILE_NAME = "farm_overlord.sqlite"
    
    var dbFilePath: String
    var databaseQueue: FMDatabaseQueue
    
    private var operationQueue: NSOperationQueue
    
    static let sharedManager = DatabaseManager()
    
    private override init() {
        let documentsPath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,
            NSSearchPathDomainMask.UserDomainMask, true)[0] as String
        
        self.dbFilePath = documentsPath.stringByAppendingString("/" + DATABASE_FILE_NAME)
        self.databaseQueue = FMDatabaseQueue(path: self.dbFilePath)
        
        self.operationQueue = NSOperationQueue()
    }
    
    func createTables() {
        if !NSFileManager().fileExistsAtPath(self.dbFilePath) {
            print("The DB does not exist at [" + self.dbFilePath + "]")
        } else {
            print("The DB exists at [" + self.dbFilePath + "]")
        }
        
        let createEmployees = UpdateOperation(sql: String(format: "%@", arguments: [SQLConstants.CREATE_TABLE_EMPLOYEES]), arguments: nil, completion: nil)
        let createTasks = UpdateOperation(sql: String(format: "%@", arguments: [SQLConstants.CREATE_TABLE_TASKS]), arguments: nil, completion: nil)
        
        self.operationQueue.addOperation(createEmployees)
        self.operationQueue.addOperation(createTasks)
    }
    
    func dropTables () {
        self.databaseQueue.inDatabase { (db) -> Void in
            let sql = String(format: "%@;%@;", arguments: [SQLConstants.DROP_TABLE_TASKS, SQLConstants.DROP_TABLE_EMPLOYESS])
            db.executeStatements(sql)
        }
    }
    
    func addOperation(operation: NSOperation) {
        self.operationQueue .addOperation(operation)
    }
}
