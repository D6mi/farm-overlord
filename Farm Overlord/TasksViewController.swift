//
//  TasksViewController.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 11/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

class TasksViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, TaskDetailDelegate {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var unassignedTasksBarButton: UIBarButtonItem?
    
    var taskGroups = [TaskGroup]()
    var selectedFilter: TaskStatus?
    
    var scopeTitles = ["title", "description", "username"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Tasks"
        self.searchBar.scopeButtonTitles = ["Title", "Description", "Employee"]
        self.searchBar.placeholder = "Minimum 3 characters"
        self.searchBar.sizeToFit()
        
        var barButtons = [UIBarButtonItem]()
        
        if AppState.sharedInstance.signedInUser?.role == EmployeeRole.Admin {
            barButtons.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: Selector("buttonAddTaskTapped:")))
            barButtons.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Organize, target: self, action: Selector("buttonFilterTapped:")))
        }
        
        self.navigationItem.setRightBarButtonItems(barButtons, animated: true)
        self.navigationItem.leftItemsSupplementBackButton = true
        
        Task.getAll { (tasks) -> (Void) in
            self.createTaskGroups(tasks)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.tableView.indexPathForSelectedRow != nil {
            self.tableView.deselectRowAtIndexPath(self.tableView.indexPathForSelectedRow!, animated: true)
        }
    }
    
    // MARK: -
    // MARK: UITableViewDataSource
    // MARK: -
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.taskGroups.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.taskGroups[section].tasks.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("taskCell", forIndexPath: indexPath) as! TaskTableViewCell
        
        let task = self.taskGroups[indexPath.section].tasks[indexPath.row]
        cell.setupCellWithTask(task)
        
        return cell
    }
    
    // MARK: -
    // MARK: UITableViewDelegate
    // MARK: -
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.taskGroups[section].title
    }
    
    func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == self.numberOfSectionsInTableView(tableView) - 1 {
            return "Signed in as \(AppState.sharedInstance.signedInUser!.username)"
        } else {
            return ""
        }
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100.0
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.Delete
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            let taskGroup = self.taskGroups[indexPath.section]
            let task = taskGroup.tasks[indexPath.row]
            
            task.remove({ (success) -> Void in
                if success {
                    taskGroup.tasks.removeAtIndex(indexPath.row)
                    
                    if taskGroup.tasks.count == 0 {
                        Task.getAll { (tasks) -> (Void) in
                            self.createTaskGroups(tasks)
                        }
                    } else {
                        self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                    }
                } else {
                    Utilites.showAlertController("Delete", message: "Failed deleting the Task.", style: UIAlertControllerStyle.Alert, actions: [], presenter: self)
                }
            })
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.searchBar.resignFirstResponder()
    }
    
    // MARK: -
    // MARK: UISearchBar
    // MARK: -
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        Task.getAll({ (tasks) -> (Void) in
            self.createTaskGroups(tasks)
        })
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        
        if searchBar.text == nil {
            searchBar.text = ""
        }
        
        let searchText: String! = "%\(searchBar.text!)%"
        
        Task.getTasks(self.scopeTitles[searchBar.selectedScopeButtonIndex], searchTerm: searchText, filter: self.selectedFilter) { (tasks) -> (Void) in
            self.createTaskGroups(tasks)
        }
    }
    
    // MARK: -
    // MARK: TaskDetailDelegate
    // MARK: -
    
    func didUpdateTask(updatedTask: Task) {
        let selectedIndexPath = self.taskIndexPath(updatedTask)
        
        if selectedIndexPath != nil {
            let taskGroup = self.taskGroups[selectedIndexPath!.section]
            taskGroup.tasks[selectedIndexPath!.row].updateTask(updatedTask)
            
            // Recreate the task groups. Will accommodate for all possible changes in the Task structure.
            Task.getAll { (tasks) -> (Void) in
                self.createTaskGroups(tasks)
            }
            
            Utilites.delay(0.25, closure: { () -> () in
                let sender = self.tableView.cellForRowAtIndexPath(selectedIndexPath!)
                
                if sender == nil {
                    self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), animated: true, scrollPosition: UITableViewScrollPosition.None)
                    self.tableView(self.tableView, didSelectRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0))
                    
                    if self.splitViewController!.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular {
                        self.performSegueWithIdentifier("segueTaskDetail", sender: self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)))
                    } else {
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                } else {
                    self.tableView.selectRowAtIndexPath(selectedIndexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
                    self.tableView(self.tableView, didSelectRowAtIndexPath: selectedIndexPath!)
                    
                    if self.splitViewController!.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular {
                        self.performSegueWithIdentifier("segueTaskDetail", sender: sender)
                    } else {
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                }
            })
        }
    }
    
    func didAddTask(task: Task) {
        let username = task.employee?.username
        let relatedTaskGroup = self.taskTaskGroup(task)
        
        guard username != nil && relatedTaskGroup != nil else {
            Task.getAll { (tasks) -> (Void) in
                self.createTaskGroups(tasks)
            }
            
            if self.splitViewController!.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular {
                self.performSegueWithIdentifier("segueTaskDetail", sender: self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)))
            } else {
                self.navigationController?.popViewControllerAnimated(true)
            }
            
            return
        }
        
        
        guard relatedTaskGroup != nil else {
            Task.getAll { (tasks) -> (Void) in
                self.createTaskGroups(tasks)
            }
            
            return
        }
        
        relatedTaskGroup?.tasks.insert(task, atIndex: relatedTaskGroup!.tasks.count)
        self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: relatedTaskGroup!.tasks.count - 1, inSection: self.taskGroups.indexOf(relatedTaskGroup!)!)], withRowAnimation: UITableViewRowAnimation.Automatic)
        
        if self.splitViewController!.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular {
            self.performSegueWithIdentifier("segueTaskDetail", sender: self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)))
        } else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    // MARK: -
    // MARK: Navigation
    // MARK: -
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueTaskDetail" {
            if sender is UITableViewCell {
                let taskDetail = (segue.destinationViewController as! UINavigationController).viewControllers.first as! TaskDetailTableViewController
                
                let indexPath = self.tableView.indexPathForCell(sender as! UITableViewCell)!
                let task = self.taskGroups[indexPath.section].tasks[indexPath.row]
                
                taskDetail.taskDetailMode = TaskDetailMode.Edit
                taskDetail.task = task
                taskDetail.delegate = self
            } else {
                let taskDetail = (segue.destinationViewController as! UINavigationController).viewControllers.first as! TaskDetailTableViewController
                
                taskDetail.taskDetailMode = TaskDetailMode.New
                taskDetail.delegate = self
            }
        }
    }
    
    // MARK: -
    // MARK: Actions
    // MARK: -
    
    func buttonAddTaskTapped(sender: UIBarButtonItem) {
        self.performSegueWithIdentifier("segueTaskDetail", sender: sender)
    }
    
    func buttonUnassignedTapped(sender: UIBarButtonItem) {
        let indexPath = NSIndexPath(forRow: 0, inSection: self.tableView.numberOfSections - 1)
        self.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
    }
    
    func buttonFilterTapped(sender: UIBarButtonItem) {
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (action) -> Void in
        }
        
        let toDoAction = UIAlertAction(title: "To do", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.selectedFilter = TaskStatus.ToDo
            self.filterTaskGroupsByStatus(self.selectedFilter)
            
            sender.tintColor = UIColor.redColor()
        }
        
        let inProgressAction = UIAlertAction(title: "In progress", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.selectedFilter = TaskStatus.InProgress
            self.filterTaskGroupsByStatus(self.selectedFilter)
            
            sender.tintColor = UIColor.yellowColor()
        }
        
        let completedAction = UIAlertAction(title: "Completed", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.selectedFilter = TaskStatus.Completed
            self.filterTaskGroupsByStatus(self.selectedFilter)
            
            sender.tintColor = UIColor.greenColor()
        }
        
        let noneAction = UIAlertAction(title: "None", style: UIAlertActionStyle.Default) { (action) -> Void in
            self.selectedFilter = nil
            self.filterTaskGroupsByStatus(self.selectedFilter)
            
            sender.tintColor = self.view.tintColor
        }
        
        if self.splitViewController!.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular && self.splitViewController!.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.Regular {
            let alertController = UIAlertController(title: "", message: "Filter Tasks by status", preferredStyle: UIAlertControllerStyle.ActionSheet)
            
            for alertActon in [cancelAction, toDoAction, inProgressAction, completedAction, noneAction] {
                alertController.addAction(alertActon)
            }
            
            alertController.popoverPresentationController?.barButtonItem = sender
            self.presentViewController(alertController, animated: true, completion: nil)
            
        } else {
            Utilites.showAlertController("", message: "Filter Tasks by status", style: UIAlertControllerStyle.ActionSheet, actions: [cancelAction, toDoAction, inProgressAction, completedAction, noneAction], presenter: self)
        }
        
    }
    
    // MARK: -
    // MARK: Helpers
    // MARK: -
    
    /**
    The main method "tasked" to retrieve, format and display TaskGroups
    
    - parameter tasks: A collection of Task instances which should be converted into TaskGroups.
    - note: This method does all of the heavy lifting by turning Task instances into TaskGroup instances which are then used to display the data in the UITableView. The method goes through all of the passed in Task instance and does the necessary filtering based on the Task's assigned Empoyee, if any.
    */
    func createTaskGroups(tasks: [Task]) {
        self.activityIndicator.startAnimating()
        
        var taskGroups = [TaskGroup]()
        var myTasks = [Task]()
        var unassignedTasks = [Task]()
        
        var temp = [String: [Task]]()
        
        for task in tasks {
            let username = task.employee?.username
            
            if username == nil {
                // Skip unassigned tasks
                unassignedTasks.append(task)
                continue
            }
            
            if username == AppState.sharedInstance.signedInUser!.username {
                // Skip my tasks
                myTasks.append(task)
                continue
            }
            
            if temp.keys.contains(task.employee!.username) {
                var innerTasks = temp[task.employee!.username]
                innerTasks!.append(task)
                temp[task.employee!.username] = innerTasks
            } else {
                temp[task.employee!.username] = [task]
            }
        }
        
        for key in temp.keys {
            let taskGroup = TaskGroup(title: key, tasks: temp[key]!)
            taskGroups.append(taskGroup)
        }
        
        if myTasks.count > 0 {
            let myTaskGroup = TaskGroup(title: "My tasks", tasks: myTasks)
            taskGroups.insert(myTaskGroup, atIndex: 0)
        }
        
        if unassignedTasks.count > 0 {
            let unassignedTaskGroup = TaskGroup(title: "Unassigned tasks", tasks: unassignedTasks)
            taskGroups.append(unassignedTaskGroup)
            
            self.unassignedTasksBarButton = UIBarButtonItem(image: UIImage(named: "down"), style: UIBarButtonItemStyle.Done, target: self, action: Selector("buttonUnassignedTapped:"))
            self.navigationItem.leftBarButtonItem = self.unassignedTasksBarButton
        } else {
            self.navigationItem.setLeftBarButtonItem(nil, animated: true)
        }
        
        self.taskGroups = taskGroups
        
        self.activityIndicator.stopAnimating()
        self.tableView.reloadData()
    }
    
    /**
    Retrives a NSIndexPath matching the passed in Task instance.
    
    - parameter task: A Task instance to retrieve a matching NSIndexPath.
    
    - returns: Returns a NSIndexPath for a given Task if exists, otherwise returns nil.
    */
    func taskIndexPath(task: Task) -> NSIndexPath? {
        for section in 0...self.taskGroups.count - 1 {
            let taskGroup = self.taskGroups[section]
            
            for row in 0...taskGroup.tasks.count - 1 {
                let innerTask = taskGroup.tasks[row]
                
                if innerTask.taskId == task.taskId {
                    return NSIndexPath(forRow: row, inSection: section)
                }
            }
        }
        
        return nil
    }
    
    /**
    Retrives a TaskGroup instance to which a certain Task instances belongs to.
    
    - parameter task: A Task instance to retrieve a matching TaskGroup.
    
    - returns: Returns a TaskGroup instance if found, otherwise returns nil.
    */
    func taskTaskGroup(task:Task) -> TaskGroup? {
        for taskGroup in self.taskGroups {
            for innerTask in taskGroup.tasks {
                let employee = task.employee
                
                guard employee != nil else {
                    return nil
                }
                
                if innerTask.employee!.username == task.employee!.username {
                    return taskGroup
                }
            }
        }
        
        return nil
    }
    
    /**
    Initiates a Task retrieval based on the filter.
    
    - parameter status: The TaskStatus to use to filter the Tasks.
    */
    func filterTaskGroupsByStatus(status: TaskStatus?) {
        if searchBar.text == nil {
            searchBar.text = ""
        }
        
        let searchText: String! = "%\(searchBar.text!)%"
        
        Task.getTasks(self.scopeTitles[searchBar.selectedScopeButtonIndex], searchTerm: searchText, filter: self.selectedFilter) { (tasks) -> (Void) in
            self.createTaskGroups(tasks)
        }
    }
    
}
