//
//  QueryOperation.swift
//  Farm Overlord
//
//  Created by Domagoj Kulundzic on 05/10/15.
//  Copyright © 2015 Domagoj Kulundzic. All rights reserved.
//

import UIKit

/**
A subclass of DatabaseOperation, represents a single query (SELECT) performed on the database.
- note: The class is only responsible for delivering the results to the caller, it DOES NOT call the passed in completion block on the main thread,
the caller's responsible for that.
*/
class QueryOperation: DatabaseOperation {
    var completion: ((result: FMResultSet, error: NSError?) -> (Void))?
    
    /**
    Initializes a new instance.
    
    - parameter sql:        The SQL query statement to run against the Database.
    - parameter arguments:  Any array of objects that will be used as arguments to the query.
    - parameter completion: A completion block to be executed once the query completed.
    - parameter error:      A NSError instance describing the error, nil if no error occurred.
    
    - returns: Returns a newly initialized instance of the QueryOpereation class.
    */
    init(sql: String, arguments: [AnyObject]?, completion: ((result: FMResultSet, error: NSError?) -> Void)) {
        self.completion = completion
        
        super.init(sql: sql, arguments: arguments)
    }
    
    override func main() {
        let queue = DatabaseManager.sharedManager.databaseQueue
        
        queue.inDatabase { (db) -> Void in
            let result = db.executeQuery(self.sql, withArgumentsInArray: self.arguments)
            
            if let completion = self.completion {
                completion(result: result, error: nil)
                result.close()
            }
        }
    }
}
